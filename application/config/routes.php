<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';

$route['painel'] = 'paginas/admin_paginas';
$route['painel/paginas'] = 'paginas/admin_paginas';
$route['painel/paginas/(:any)'] = 'paginas/admin_paginas/$1';
$route['painel/slideshow'] = 'slideshow/admin_slideshow';
$route['painel/slideshow/(:any)'] = 'slideshow/admin_slideshow/$1';
$route['painel/contato'] = 'contato/admin_contatos';
$route['painel/atuacao'] = 'servicos/admin_servicos';
$route['painel/atuacao/(:any)'] = 'servicos/admin_servicos/$1';
$route['painel/noticias'] = 'noticias/admin_noticias';
$route['painel/noticias/(:any)'] = 'noticias/admin_noticias/$1';
$route['painel/perfis'] = 'colaboradores/admin_colaboradores';
$route['painel/perfis/(:any)'] = 'colaboradores/admin_colaboradores/$1';

$route['empresa'] = 'paginas/view/empresa';
$route['perfil'] = 'paginas/view/perfil';
$route['atuacao'] = 'paginas/view/atuacao';




/* End of file routes.php */
/* Location: ./application/config/routes.php */