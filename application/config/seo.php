<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'Fernandes & Pedrosa Advogados';

$config['site_decription'] = 'Fernandes & Pedrosa Advogados é um escritório 
								completo, exploramos os mais variados 
								ramos do direito em todos os seus aspectos.';

$config['site_keywords'] = 'Fernandes & Pedrosa Advogados, Fernandes, Pedrosa,
							advogados, direito, advocacia, escritório';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';

/* End of file seo.php */
/* Location: ./application/config/seo.php */