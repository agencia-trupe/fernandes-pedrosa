<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Controller Class
 *
 *
 * @package Base Project
 * @subpackage  Controllers
 */
class MY_Controller extends MX_Controller {
    public function __construct() {
        parent::__construct();
    }
}

class AuthController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri', uri_string());
            $this->session->set_flashdata('message', 'You need to log in.');
            redirect('/login');
        }
    }
}