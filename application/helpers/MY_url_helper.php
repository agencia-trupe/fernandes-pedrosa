<?php

// ------------------------------------------------------------------------

/**
 * Site Name
 *
 * Returns the "site_name" item from your config file
 *
 * @access    public
 * @return    string
 */    
function site_name()
{
    $CI =& get_instance();
    return $CI->config->item('site_name');
} 