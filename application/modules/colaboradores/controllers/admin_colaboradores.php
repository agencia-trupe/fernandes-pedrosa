<?php
/**
* File: admin_colaboradores.php
* 
* PHP version 5.3
*
* @category Fernandes_Pedrosa
* @package  Fernandes_Pedrosa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_colaboradores
 * 
 * @category Fernandes_Pedrosa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_colaboradores extends AuthController
{
	/**
	 * Array de variáveis para a view.
	 * 
	 * @var array.
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['module'] = 'perfis';
		$this->load->model('colaboradores/colaborador');
	}

	/**
	 * Função de página principal
	 * 
	 * @return void página principal
	 */
	public function index()
	{
		$this->lista();
	}

	/**
	 * Lista os colaboradores
	 * 
	 * @return void view
	 */
	public function lista()
	{
		$this->data['colaboradores'] = $this->colaborador->get_all();
		$this->data['conteudo'] = 'colaboradores/admin_lista';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Edita um colaborador
	 * 
	 * @param int $colaborador_id id do colaborador
	 * 
	 * @return void view
	 */
	public function editar($colaborador_id)
	{
		$this->load->model('colaboradores/colaborador');
		$this->data['colaborador'] = $this->colaborador
									->get_conteudo($colaborador_id, 'id');
		$this->data['acao'] = 'editar';
		$this->data['conteudo'] = 'colaboradores/admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Cadastra um novo colaborador
	 * 
	 * @return void view
	 */
	public function cadastrar()
	{
		$this->data['acao'] = 'cadastrar';
		$this->data['conteudo'] = 'colaboradores/admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Processa o cadastro de um novo colaborador
	 * 
	 * @return void redirect
	 */
	public function processa()
	{
		$this->data['acao'] = 'editar';
		$colaborador = $this->colaborador->get_conteudo($this->input->post( 'id' ), 'id');

		if( ! $this->form_validation->run('colaborador') )
		{
			$this->data['colaborador'] = $colaborador;
			$this->data['conteudo'] = 'colaboradores/admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$post = array();
			foreach($_POST as $chave => $valor)
			{
				$post[$chave] = $valor;
			}

			//Checa se houve envio de imagem
			if(strlen($_FILES['imagem']['name'])>0)
			{
				//Tenta persistir a imagem e 
				//retorna uma exception em caso de erro
				try 
				{
					$upload = $this->_upload_image();
					$post['imagem'] = $upload['file_name'];
				} 
				catch (Exception $e) 
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage(), $colaborador->id, 'id');
				}
			}

			$post['updated'] = time();
			if($this->colaborador->change($post))
			{
				$this->session->set_flashdata('success', 'Registro alterado com sucesso');
				redirect('painel/perfis');
			}
			else
			{
				$this->session->set_flashdata('error', 'Não foi possível alterar o registro.
					Tente novamente ou entre em contato com o suporte');
				redirect('painel/perfis/edita/' . $post['id']);
			}
		}
	}

	/**
	 * Processa o cadastro de um novo colaborador
	 * 
	 * @return void redirect
	 */
	public function processa_cadastro()
	{
		$this->data['acao'] = 'cadastrar';

		if( ! $this->form_validation->run('colaborador') )
		{
			$this->data['conteudo'] = 'colaboradores/admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$post = array();
			foreach($_POST as $chave => $valor)
			{
				$post[$chave] = $valor;
			}

			//Checa se houve envio de imagem
			if(strlen($_FILES['imagem']['name'])>0)
			{
				//Tenta persistir a imagem e retorna uma exception caso não persista
				try 
				{
					$upload = $this->_upload_image();
					$post['imagem'] = $upload['file_name'];
				} 
				catch (Exception $e) 
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage());
				}
			}

			if(is_null($post['imagem']))
			{
				$this->session->set_flashdata('error', 'Você precisa selecionar uma imagem.');
				redirect('painel/perfis/cadastrar');
			}
			elseif($this->colaborador->insert($post))
			{
				$this->session->set_flashdata('success', 'Registro adicionado com sucesso');
				redirect('painel/perfis');
			}
			else
			{
				$this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
					Tente novamente ou entre em contato com o suporte');
				redirect('painel/perfis/cadastrar');
			}
		}
	}

	/**
	 * Remove o registro de um colaborador
	 * 
	 * @param int $colaborador_id id do colaborador.
	 * 
	 * @return void redirect
	 */
	public function apagar($colaborador_id)
	{
		$apaga = $this->colaborador->apaga($colaborador_id);
		if($apaga)
		{
			$this->session->set_flashdata('success', 'Registro removido com sucesso');
			redirect('painel/perfis');
		}
		else
		{
			$this->session->set_flashdata('error', 'Não foi possível remover o registro.
				Tente novamente ou entre em contato com o suporte');
			redirect('painel/perfis/');
		}
	}

	/**
	 * Reordena os tipos de projetos para a exibição
	 * 
	 * @return void status do processamento
	 */
	public function sort_colaboradores()
	{
		$itens = $this->input->post('colaborador');
		if ($itens)
		{
			$ordenar = $this->colaborador->ordena($itens);
			if($ordenar)
			{
				echo 'Ordenado';
			}
			else
			{
				echo 'Erro!';
			}
		} 
		else 
		{
			echo 'Erro!';
		}
	}

	/**
	 * Retorna o formulário contendo a mensagem de erro
	 * 
	 * @param string $acao       [cadastro|edição]
	 * @param string $error      mensagem de erro
	 * @param mixed  $value      valor a buscar no banco de dados
	 * @param string $identifier identificador do valor passado
	 * 
	 * @return void formulário
	 */
	private function _form_error_return($acao, $error = NULL, $value = NULL, $identifier = NULL)
	{
		if( $acao === 'editar' )
			$this->data['colaborador'] = $this->colaborador->get_conteudo($value, $identifier);
		
		if( $error )
			$this->data['error'] = $error;

		$this->data['module'] = 'noticias';
		$this->data['acao'] = $acao;
		$this->data['conteudo'] = 'admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Realiza o upload de uma imagem
	 *
	 * @throws Exception If cant do upload
	 * @return void dados de upload
	 */
	private function _upload_image()
	{
		$config['upload_path'] = './assets/img/perfis/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '8000';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('imagem') )
			throw new Exception($this->upload->display_errors(), 1);

		$this->load->library('image_moo');
		//Is only one file uploaded so it ok to use it with $uploader_response[0].
		$upload_data = $this->upload->data();
		$file_uploaded = $upload_data['full_path'];
		//Locais dos arquivos alterados
		$new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

		if ( ! $this->image_moo->load($file_uploaded)
								->resize_crop(220, 220)
								->save($new_file, TRUE) )
			throw new Exception('Erro ao redimensionar imagem', 1);
			
		return $this->upload->data();

	}
}
/* End of file admin_colaboradores.php */
/* Location: ./modules/colaboradores/controllers/admin_colaboradores.php */