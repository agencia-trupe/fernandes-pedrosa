<?php
/**
* File: colaboradores.php
* 
* PHP version 5.3
*
* @category Fernandes_Pedrosa
* @package  Fernandes_Pedrosa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Colaboradores
 * 
 * @category Fernandes_Pedrosa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Colaboradores extends MX_Controller
{
	/**
	 * Array de variáveis para a view.
	 * 
	 * @var array;
	 */
	var $data;

	/**
	 * Exibe uma lista de colaboradores em outra página
	 * 
	 * @return void lista html
	 */
	public function parcial()
	{
		$this->load->model('colaboradores/colaborador');
		$colaboradores = $this->colaborador->get_all();
		$this->data['colaboradores'] = array_chunk($colaboradores, 2);
		$this->data['conteudo'] = 'colaboradores/parcial';
		$this->load->view($this->data['conteudo'], $this->data);
	}
}
/* End of file colaboradores.php */
/* Location: ./modules/colaboradores/controllers/colaboradores.php */