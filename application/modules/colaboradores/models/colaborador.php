<?php
/**
* File: colaborador.php
* 
* PHP version 5.3
*
* @category Fernandes_Pedrosa
* @package  Fernandes_Pedrosa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Colaborador
 * 
 * @category Fernandes_Pedrosa
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Colaborador extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'colaboradores';

	/**
	 * Retorna todos os registros do banco de dados
	 * 
	 * @return array registros
	 */
	function get_all()
	{
		$colaborador = new Colaborador();
		$colaborador->order_by('ordem', 'ASC')->get();
		$result = array();
		foreach($colaborador->all as $colaborador)
		{
			$result[] = $colaborador;
		}
		if(sizeof($result))
		{
			return $result;
		}
		return FALSE;
	}

	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo($valor, $tipo)
	{
		$colaborador = new Colaborador();
		$colaborador->where($tipo, $valor)->get();

		if( $colaborador->exists() )
		{
			return $colaborador;
		}
		return NULL;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$colaborador = new Colaborador();
		$colaborador->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update_data['updated'] = time();
		$update = $colaborador->update($update_data);
		if($update)
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Insere um novo registro no banco
	 * 
	 * @param array $dados dados do registro.
	 * 
	 * @return boolean status
	 */
	function insert($dados)
	{
		$colaboradores = new Colaborador();
		$colaboradores->select_max('ordem')->get();

		$colaborador = new Colaborador();
		foreach ($dados as $chave => $valor)
		{
			$colaborador->$chave = $valor;
		}
		$colaborador->ordem = $colaborador->ordem + 1;
		
		$colaborador->created = time();
		if($colaborador->save())
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Remove um registro do banco de dados
	 * 
	 * @param int $colaborador_id id do colaborador.
	 * 
	 * @return boolean status
	 */
	function apaga($colaborador_id)
	{
		$colaborador = new Colaborador();
		$colaborador->where('id', $colaborador_id)->get();
		if($colaborador->delete())
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Ordena os dados para exibição
	 * 
	 * @param array $dados array com dados para ordenar
	 * 
	 * @return boolean status
	 */
	function ordena($dados)
	{
		$result = array();
		foreach($dados as $chave => $valor)
		{
			$colaborador = new Colaborador();
			$colaborador->where('id', $valor);
			$update_data = array(
				'ordem' => $chave
				);
			if($colaborador->update($update_data))
			{
				$result[] = $valor;
			}
		}
		if(sizeof($result))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
/* End of file colaborador.php */
/* Location: ./modules/colaboradores/models/colaborador.php */