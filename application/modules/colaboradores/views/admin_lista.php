<div class="row-fluid">
    <div class="span9">
        <legend>Perfis 
                <?php echo anchor('painel/perfis/cadastrar', 'Novo', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-perfis btn btn-mini btn-info">ordenar ítens</a>
                <a href="#" class="salvar-ordem-perfis hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nome</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php if($colaboradores): ?>
            <?php foreach ($colaboradores as $colaborador): ?>
                <tr id="colaborador_<?php echo $colaborador->id ?>">
                    <td><?=$colaborador->nome; ?></td>
                    <td><?=anchor('painel/perfis/editar/' . $colaborador->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                    <?=anchor('painel/perfis/apagar/' . $colaborador->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>