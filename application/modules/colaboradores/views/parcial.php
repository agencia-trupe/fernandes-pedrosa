<?php foreach ($colaboradores as $chave => $perfil): ?>
	<div class="perfis-dupla">
		<div class="perfil-esquerda left">
			<div class="perfil-imagem">
				<img src="<?php echo base_url('assets/img/perfis/' . $perfil[0]->imagem) ?>" alt="<?php echo $perfil[0]->nome ?>">
			</div>
			<h2><?php echo $perfil[0]->nome ?></h2>
			<div class="perfil-texto">
				<?php echo $perfil[0]->texto ?>
			</div>
		</div>
		<?php if (isset($perfil[1])): ?>
		<div class="perfil-direita right">
			<div class="perfil-imagem">
				<img src="<?php echo base_url('assets/img/perfis/' . $perfil[1]->imagem) ?>" alt="<?php echo $perfil[1]->nome ?>">
			</div>
			<h2><?php echo $perfil[1]->nome ?></h2>
			<div class="perfil-texto">
				<?php echo $perfil[1]->texto ?>
			</div>
		</div>
		<?php endif ?>
	</div>
	<div class="clearfix"></div>
<?php endforeach ?>