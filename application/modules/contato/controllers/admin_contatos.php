<?php
/**
* File: admin_contatos.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_contatos
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_contatos extends MX_Controller
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['module'] = 'contato';
		$this->load->model('contato/endereco');
	}

	/**
	 * Função principal.
	 * 
	 * @return void página principal.
	 */
	public function index()
	{
		$this->editar();
	}

	/**
	 * Edita as informações de endereço
	 *  
	 * @return void formulário de edição de página.
	 */
	public function editar()
	{
		if ( ! $this->tank_auth->is_logged_in() ) 
		{
			$this->session->set_userdata('bounce_uri',
				$this->uri->uri_string());
			$this->data['main_content'] = 'system/mustLogin';
			$this->data['title'] = site_name() . ' - Painel de Controle';
			$this->load->view('start/templatenonav', $this->data);
		}
		else
		{
			$this->data['contato'] = $this->endereco->get_info();
			$this->data['conteudo'] = 'contato/admin_edita';
			$this->load->view('start/template', $this->data);
		}
	}

	/**
	 * Processa a atualização do endereço
	 * 
	 * @return void redirecionamento
	 */
	public function processa()
	{
		if ( ! $this->tank_auth->is_logged_in() ) 
		{
			$this->session->set_userdata('bounce_uri',
				$this->uri->uri_string());
			$this->data['main_content'] = 'system/mustLogin';
			$this->data['title'] = site_name() . ' - Painel de Controle';
			$this->load->view('start/templatenonav', $this->data);
		}
		else
		{
			if( ! $this->form_validation->run('endereco') )
			{
				$this->data['contato'] = $this->endereco->get_info();
				$this->data['conteudo'] = 'contato/admin_edita';
				$this->load->view('start/template', $this->data);
			}
			else
			{
				$post = array();
				foreach($_POST as $chave => $valor)
				{
					$post[$chave] = $valor;
				}
				if($this->endereco->change($post))
				{
					$this->session->set_flashdata('success', 'Registro alterado com sucesso');
					redirect('painel/contato');
				}
				else
				{
					$this->session->set_flashdata('error', 'Não foi possível alterar o registro.
						Tente novamente ou entre em contato com o suporte');
					redirect('painel/contato');
				}
			}
		}
	}
}
/* End of file admin_contatos.php */
/* Location: ./modules/contato/controllers/admin_contatos.php */