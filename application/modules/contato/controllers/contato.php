<?php
/**
* File: contato.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Contato
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
Class Contato extends MX_Controller
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('contato/endereco');
	}

	/**
	 * Exibe o formulário de contato.
	 * 
	 * @return void página principal.
	 */
	public function index()
	{
		$seo_config = array(
			'title' => 'Contato',
			'description' => 'Entre em contato com o ' . site_name()
			);
		$this->load->library('seo', $seo_config);
		$this->data = array(
			'titulo' => 'Contato',
			'page_title' => 'Contato',
			'page_description' => 'Uma maneira rápida de solicitar informações
			ou visita na sua empresa',
			'pagina' => 'contato',
			'conteudo' => 'contato/index'
				);
		$this->data['contato'] = $this->endereco->get_info();
		$this->load->view('layout/template', $this->data);
	}

	/**
	 * Checa o envio do formulário via AJAX
	 * 
	 * @return json objeto com status e mensagem
	 */
	function ajax_check() 
	{
		$this->load->library('form_validation');
		$this->load->library('typography');
		$this->form_validation->set_error_delimiters('', '');

		if($this->input->post('ajax') === '1') 
		{
			if($this->form_validation->run('contato') === FALSE) 
			{
				echo validation_errors();
			} 
			else 
			{
				$this->load->library('MY_PHPMailer');
				$mail = new PHPMailer();

				//Define os dados do servidor e tipo de conexão
				$mail->IsSMTP(); // Define que a mensagem será SMTP
				$mail->Host = 'localhost'; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
				$mail->SMTPAuth = TRUE; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
				$mail->Username = 'nilton@trupe.net'; // Usuário do servidor SMTP (endereço de email)
				$mail->Password = '6feynman'; // Senha do servidor SMTP (senha do email usado)
				
				$mail->Host = 'smtp.trupe.net';
				$mail->Port = '587';

				// Define o remetente
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->From = 'nilton@trupe.net'; // Seu e-mail
				$mail->Sender = 'nilton@trupe.net'; // Seu e-mail
				$mail->FromName = $this->input->post('nome'); // Seu nome

				// Define os destinatário(s)
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->AddAddress('nilton@trupe.net', 'Nilton');
				//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
				//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

				// Define os dados técnicos da Mensagem
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->IsHTML(TRUE); // Define que o e-mail será enviado como HTML
				$mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)

				// Define a mensagem (Texto e Assunto)
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->Subject  = 'Contato do site ' . site_name(); // Assunto da mensagem
				
				$data['dados'] = array(
									'nome' => $this->input->post('nome'),
									'email' => $this->input->post('email'),
									'telefone' => $this->input->post('telefone'),
									'mensagem' => $this->typography->auto_typography($this->input->post('mensagem')),
							);
				$email_view = $this->load->view('contato/email', $data, TRUE);

				$mail->Body = $email_view;


				if( ! $mail->Send() ) 
				{
					echo 'Erro: ' . $mail->ErrorInfo;
				} 
				else
				{
					echo 'Mensagem enviada com sucesso!';
				}
			}
		}
	}

	/**
	 * Exibe os links para as redes sociais
	 * 
	 * @return void view parcial
	 */
	public function social()
	{
		$this->data['contato'] = $this->endereco->get_info();
		$this->load->view('contato/social', $this->data);
	}

	/**
	 * Exibe o endereço no rodapé
	 * 
	 * @return void view parcial
	 */
	public function footer()
	{
		$this->data['contato'] = $this->endereco->get_info();
		$this->load->view('contato/footer', $this->data);
	}
}
/* End of file contato.php */
/* Location: ./modules/contato/controllers/contato.php */