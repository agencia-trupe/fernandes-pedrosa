<?php
/**
* File: endereco.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Class_name
 * 
 * @category Sisconeto
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
Class Endereco extends Datamapper{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'contatos';

	/**
	 * Construtor
	 */
	public function __construct()
	{
		// model constructor
		parent::__construct();
	}

	/**
	 * Obtem as informações de contato
	 * 
	 * @return array informaçoes
	 */
	function get_info()
	{
		$endereco = new Endereco();
		$endereco->where('id', '1');
		$endereco->limit(1);
		$endereco->get();

		return $endereco;
	}

	/**
	 * Atualiza as informações de contato
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$endereco = new Endereco();
		$endereco->where('id', $dados['id'])->get();
		$update = $endereco->update(array(
				'telefone' => $dados['telefone'],
				'telefone2' => $dados['telefone2'],
				'email' =>  $dados['email'],
				'ddd' =>  $dados['ddd'],
				'ddd2' =>  $dados['ddd2'],
				'endereco' =>  $dados['endereco'],
				'bairro' =>  $dados['bairro'],
				'cidade' =>  $dados['cidade'],
				'cep' =>  $dados['cep'],
				'uf' =>  $dados['uf'],
				'facebook' =>  $dados['facebook'],
				'twitter' =>  $dados['twitter'],
				'linkedin' =>  $dados['linkedin'],
			));
		return $update;
	}
}
/* End of file endereco.php */
/* Location: ./modules/contato/models/endereco.php */