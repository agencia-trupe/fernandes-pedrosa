<div class="conteudo-contato">
    <div class="mapa">
        <iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Maria+Paula,+62,+S%C3%A3o+Paulo,+Rep%C3%BAblica+Federativa+do+Brasil&amp;aq=0&amp;oq=rua+maria+paula,+62&amp;sll=37.0625,-95.677068&amp;sspn=40.409448,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Maria+Paula,+62+-+Rep%C3%BAblica,+S%C3%A3o+Paulo,+01319-000,+Rep%C3%BAblica+Federativa+do+Brasil&amp;t=m&amp;ll=-23.550769,-46.638508&amp;spn=0.022031,0.036478&amp;z=14&amp;output=embed&amp;iwloc=near"></iframe>
    </div>
    <div class="interna">
        <div class="contatos left">
            <h1>Contato</h1>
            <div class="separador-contato"></div>
            <span class="telefone">
                <small>(<?=$contato->ddd; ?>)</small> <?=$contato->telefone; ?>
            </span>
            <br>
            <span class="telefone">
                <small>(<?=$contato->ddd2; ?>)</small> <?=$contato->telefone2; ?>
            </span>
            <p class="endereco"><?=$contato->endereco; ?><br>
            <?=$contato->cep; ?> &middot; <?=$contato->bairro; ?><br>
            <?=$contato->cidade; ?>/<?=$contato->uf; ?>
            </p>
            <span class="email"><a href="mailto://<?=$contato->email ?>"><?=$contato->email; ?></a></span>
        </div>
        <div class="form right">
            <h1>Envie-nos uma mensagem</h1>
            <div class="separador-contato"></div>
            <?php echo form_open('', 'id="contato-form"'); ?>
                <div class="inputs">
                    <?=form_input(array('name'=>'nome','value'=>'','class'=>'nome_form textbox', 'placeholder'=>'Nome', 'title'=>'Nome'))?>
                    <?=form_input(array('name'=>'email','value'=>'','class'=>'email_form textbox', 'placeholder'=>'Email', 'title'=>'Email'))?>
                    <?=form_input(array('name'=>'telefone','value'=>'','class'=>'telefone_form textbox', 'placeholder'=>'Telefone', 'title'=>'Telefone'))?>
                </div>
                <?=form_textarea(array('name'=>'mensagem','value'=>'','class'=>'mensagem_form mensagem_contato textbox', 'placeholder'=>'Mensagem', 'title'=>'mensagem'))?><br />
                <div class="clearfix"></div>
                <input type="submit" name="submit" value="enviar" id="contato-submit">   
            <?=form_close("\n")?>
            <div class="clearfix"></div>
            </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
