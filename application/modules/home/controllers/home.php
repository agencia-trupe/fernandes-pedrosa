<?php
/**
* File: home.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Class_name
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Home extends MX_Controller
{	
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$seo_config['title'] = 'Home';
		$seo_config['description'] = 'Nosso Escritório Cria Projetos Residenciais e Gerencia visando uma boa execução de mão de obra especializada priorizando o investimento do Cliente tornando o ambiente funcional, moderno e elegante. Criamos também Projetos Coorporativos Planejando a Área de Trabalho pensando na confiança e dinamismo que toda empresa deve passar aos seus clientes e colaboradores.';
		$seo_config['keywords'] = 'planejamento físico e financeiro, layout, personalizado, moderno, contemporâneo, funcional, estilo, combinação, elegante, automação, aconchego, arte, cores, tendência, combinação, paisagismo, clean, decoração, designer de interiores, arquitetura, reforma de ambientes,  acompanhamento de obras, projetos de arquitetura, como contratar um profissional de decoração';
		$this->load->library('seo', $seo_config);
		$this->data['pagina'] = 'home';
		$this->load->model('slideshow/slide');
		$this->load->model('paginas/pagina');
		$this->load->model('noticias/noticia');
		$this->load->model('servicos/servico');
		$this->load->helper('blog');
		$this->load->helper('date');
	}

	/**
	 * Função principal.
	 * 
	 * @return void página principal.
	 */
	public function index()
	{
		$institucional = $this->pagina->get_conteudo('institucional', 'slug');

		$this->data['noticias'] = $this->noticia->get_all(2, NULL);

		$this->data['slides'] = $this->slide->get_all();

		$this->data['servicos'] = $this->servico->get_all();

		$this->load->model('paginas/pagina');
		$home = $this->pagina->get_conteudo('home', 'slug');
		$this->data['frase_home'] = $home->texto;

		$this->data['conteudo'] = 'home/index';
		$this->load->view('layout/template', $this->data);
	}

	public function template() {
		$template = array();
		$template['name'] = 'newsWidget';
		$template['template'] = '<div class="home-noticia">
        <div class="home-noticia-data">
            {{data}}
        </div>
        <div class="home-noticia-titulo">
            {{titulo}}
        </div>
        <div class="home-noticia-resumo">
            {{{resumo}}}
        </div>
        <a href="http://sisconeto.dev/novidades/?noticia={{$noticia->id}}" class="home-mais">Saiba mais</a>
    </div>';

    	echo json_encode($template);
	}
}
/* End of file home.php */
/* Location: ./modules/home/controllers/home.php */