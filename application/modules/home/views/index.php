<div class="conteudo home conteudo-home">
	<div class="faixa-cinza">
		<div class="home-slider-wrapper">
					<div class="home-slider">
						<?php foreach ($slides as $slide): ?>
							<div class="home-slide">
								<div>
									<a target="_blank" href="<?php echo $slide->link ?>" class="home-slide-image-wrapper">
										<img src="<?php echo base_url('assets/img/slides/' . $slide->imagem) ?>" alt="<?php echo $slide->titulo ?>">
									</a>
									<span class="home-slide-content">
										<h1>
											<a target="_blank" href="<?php echo $slide->link ?>">
												<?php echo $slide->titulo ?>
											</a>
										</h1>
										<a target="_blank" href="<?php echo $slide->link ?>">
											<?php echo $slide->texto ?>
										</a>
									</span>
								</div>
							</div>
						<?php endforeach ?>
					</div>
		</div>
	</div>
	<div class="faixa-verde">
		<div class="interna">
			<div class="clearfix"></div>
			<div class="frase-home">
				<?php echo $frase_home ?>
			</div>
		</div>
	</div>
	<div class="interna">
		<div class="noticias left">
			<?php echo Modules::run('noticias/widget') ?>
		</div>
		<div class="atuacao right">
			<h1>Áreas Atuação</h1>
			<?php echo Modules::run('servicos/parcial', TRUE) ?>
		</div>
	</div>
</div>
