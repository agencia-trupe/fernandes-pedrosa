<?php
/**
* File: noticias.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Noticias
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Noticias extends MX_Controller
{
	/**
	 * Array de variáveis passado para a view
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['pagina'] = 'novidades';
		$this->load->model('noticias/noticia');
		$this->load->helper('date');
	}

	public function index()
	{
		$seo_config['title'] = 'Novidades';
		$seo_config['description'] = 'Novidades do escritório Sisconeto Advogados Associados';
		$this->load->library('seo', $seo_config);

		$noticias = $this->noticia->get_all();
		$this->data['nav_links'] = array_chunk($noticias, 4);
		$this->data['conteudo'] = 'index';
		$this->load->view('layout/template', $this->data);
	}

	public function detalhe($noticia_id)
	{
		if($this->input->is_ajax_request())
		{
			$this->load->helper('date');

			$result = $this->noticia->get_conteudo($noticia_id, 'id');

			$noticia = array();
			$noticia['data'] = get_long_date($result->published);
			$noticia['titulo'] = $result->titulo;
			$noticia['texto'] = utf8_encode($result->texto);
			$noticia['resumo'] = utf8_encode($result->resumo);
			$noticia['imagem'] = utf8_encode($result->imagem);

			echo json_encode($noticia);
		}
		else
		{
			redirect();
		}
	}

	public function json()
	{
		$this->load->helper('blog');
        $noticia = new Noticia();
        $noticia->where('published <', time());
        $noticia->order_by('published', 'desc');
        $noticia->get();

        $arr = array();
        foreach($noticia->all as $noticias)
        {
        	$arr[$noticias->id]['id'] = $noticias->id;
        	$arr[$noticias->id]['data'] = get_long_date($noticias->published);
            $arr[$noticias->id]['titulo'] = $noticias->titulo;
            $arr[$noticias->id]['resumo'] = get_excerpt($noticia->texto, 24);
        }

        echo json_encode(array_chunk($this->_shuffle_assoc($arr),2));
	}

	private function  _shuffle_assoc($list) 
	{ 
		if (!is_array($list)) return $list; 

		$keys = array_keys($list); 
		shuffle($keys); 
		$random = array(); 
		foreach ($keys as $key) 
		  $random[$key] = $list[$key]; 

		return $random; 
	}

	public function template()
	{
		$template = array();
		$template['name'] = 'noticia_template';
		$template['template'] = '<div class="conteudo-inner">
				<header>
                <div class="data">{{data}}</div>
                <div class="separador-data"></div>
                <h1>{{titulo}}</h1>
                <div class="separador-titulo"></div>
                </header>
                {{#imagem}}
                    <img src="http://sisconeto.dev/assets/img/noticias/{{imagem}}">
                {{/imagem}}
                {{{texto}}}
            </div>';

        echo json_encode($template);
	}

	public function widget()
	{
		$data['noticias'] = $this->noticia->get_all(3);
		$this->load->view('noticias/widget', $data);
	}
}