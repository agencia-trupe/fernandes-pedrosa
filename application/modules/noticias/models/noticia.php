<?php
/**
* File: noticia.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Noticia
 * 
 * @category Sisconeto
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Noticia extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'noticias';

	/**
	 * Retorna todos os registros do banco de dados
	 * 
	 * @return array registros
	 */
	function get_all($limit = NULL, $offset = NULL)
	{
		$noticia = new noticia();
		$noticia->where('published <= ', time());
		$noticia->order_by('published', 'DESC');
		if($limit)
		{
			$noticia->get($limit, $offset);
		} 
		else
		{
			$noticia->get();
		}
		
		$result = array();
		foreach($noticia->all as $noticia)
		{
			$result[] = $noticia;
		}

		return (sizeof($result)) ? $result : NULL;
		
	}

	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo( $valor, $tipo )
	{
		$noticia = new noticia();
		$noticia->where( $tipo, $valor )->get();
		if( ! $noticia->exists() )
			throw new Exception("Nenhum registro corresponde à esse id", 1);

		return $noticia;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$noticia = new noticia();
		$noticia->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update = $noticia->update($update_data);
		if( ! $update )
			throw new Exception("Erro ao atualizar registro", 1);
			
		return TRUE;
	}

	/**
	 * Insere um novo registro no banco
	 * 
	 * @param array $dados dados do registro.
	 * 
	 * @return boolean status
	 */
	function insert($dados)
	{
		$noticia = new noticia();
		foreach ($dados as $chave => $valor)
		{
			$noticia->$chave = $valor;
		}
		$insert = $noticia->save();

		if( ! $insert )
			throw new Exception("Erro ao inserir notícia.", 1);
			
		return true;
	}

	/**
	 * Remove um registro do banco de dados
	 * 
	 * @param int $noticia_id id do serviço.
	 * 
	 * @return boolean status
	 */
	function apaga($noticia_id)
	{
		$noticia = new noticia();
		$noticia->where('id', $noticia_id)->get();
		$imagem = $noticia->imagem;
		if( ! $noticia->delete() )
			throw new Exception("Erro ao apagar registro", 1);
			

		return $imagem;
	}
}
/* End of file noticia.php */
/* Location: ./modules/noticias/models/noticia.php */