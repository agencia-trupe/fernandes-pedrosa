<div class="row-fluid">
    <div class="span9">
        <?php if(isset($error)): ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div>
    <?php endif; ?> 
       <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Notícia</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/noticias/edit';
                    break;
                
                default:
                    $action = 'painel/noticias/add';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?=form_hidden( 'id', ( $acao == 'editar') ? $noticia->id : '' ); ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar') ? $noticia->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
    <?=form_label('Data de publicação'); ?>
    <?=form_input(array(
        'name' => 'published',
        'value' => set_value('published', ( $acao == 'editar') ? date('d/m/Y', $noticia->published) : ''),
        'class' => 'span5',
        'id' => 'datepicker',
    )); ?>
    <?=form_error('published'); ?>
    <?=form_label('Resumo'); ?>
    <?=form_textarea(array(
        'name' => 'resumo',
        'value' => set_value('resumo', ( $acao == 'editar') ? $noticia->resumo : ''),
        'class' => 'span5 tinymce'
    )); ?>
    <?=form_error('titulo'); ?>
    <?=form_label('Descrição'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', ( $acao == 'editar') ? $noticia->texto : ''),
        'class' => 'span10 tinymce'
    )); ?>
    <?=form_error('texto'); ?>
    <br>
    <?php if(isset($noticia->imagem)):?>
    <img width="200px" src="<?php echo base_url(); ?>assets/img/noticias/<?php echo $noticia->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="imagem">Alterar Imagem</label>
        <div class="controls">
            <?php echo form_upload('imagem', set_value('imagem')); ?>
            <span class="help-inline"><?php echo form_error('imagem'); ?></span>
        </div>
    </div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/noticias', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?> 
    </div>
</div>