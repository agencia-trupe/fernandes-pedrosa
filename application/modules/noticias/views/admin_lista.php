<div class="row-fluid">
    <div class="span9">
         <legend>noticias <a class="btn btn-info btn-mini" href="<?=site_url('painel/noticias/add'); ?>">Novo</a></legend>
         <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Data de publicação</th><th class="span8">Título</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
        <?php if($noticias): ?>
            <?php foreach ($noticias as $noticia): ?>
                <tr>
                    <td><?php echo date('d/m/y', $noticia->published); ?></td>
                    <td><?=$noticia->titulo; ?></td>
                    <td>
                        <?=anchor('painel/noticias/edit/' . $noticia->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?php echo form_open('painel/noticias/delete', 'style="display:inline"') ?>
                            <?php echo form_hidden('id', $noticia->id) ?>
                            <?php echo form_submit('', 'apagar', 'class="btn btn-mini btn-danger"') ?>
                        <?php echo form_close() ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>