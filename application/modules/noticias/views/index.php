<div class="conteudo pagina noticias conteudo-noticias">
	<h1>
		<div class="interna">
			Notícias
		</div>
	</h1>
	<div class="interna">
		<div class="navegacao left">
		<div class="nav-wrapper">
			<div class="novidades-nav-slider">
			<?php foreach ($nav_links as $chave => $valores): ?>
				<div class="novidades-nav-slide">
					<?php foreach ($valores as $link): ?>
						<a href="<?php echo site_url('noticias/?page=' . $chave . '&noticia=' . $link->id) ?>" class="nav-link" data-page="<?php echo $chave ?>" data-id="<?php echo $link->id ?>">
							<div class="nav_link-content">
								<span class="data">
									<span class="dia">
										<?php echo date('d', $link->published) ?>
									</span>
									<span class="mes">
										<?php echo month_name($link->published, TRUE, TRUE) ?>
									</span>
								</span>
								<span class="titulo">
									<?php echo $link->titulo ?>
								</span>
								<div class="clearfix"></div>
							</div>
							<span class="barra-nav-link"></span>
						</a>
						<div class="separador"></div>
					<?php endforeach ?>
				</div>
			<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="conteudo-outer right">
		<div class="conteudo">
			<div class="conteudo-wrapper">
				<div class="conteudo-inner">
					<header>
						<div class="data"><?php echo get_long_date($nav_links[0][0]->published) ?></div>
						<div class="separador-data"></div>
						<h1><?php echo $nav_links[0][0]->titulo ?></h1>
						<div class="resumo"><?php echo $nav_links[0][0]->resumo ?></div>
						<div class="separador-resumo"></div>
					</header>
					<?php if($nav_links[0][0]->imagem): ?>
						<div class="noticia-image-wrapper" style="text-align:center">
							<img src="<?php echo base_url('assets/img/noticias/' . $nav_links[0][0]->imagem) ?>" alt="<?php echo $nav_links[0][0]->titulo ?>">			
						</div>
					<?php endif; ?>
					<?php echo $nav_links[0][0]->texto ?>
				</div>
			</div>
		</div>
	</div>	
	</div>
</div>