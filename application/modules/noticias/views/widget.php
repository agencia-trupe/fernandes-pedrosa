<div class="noticias-widget">
	<h1>Notícias <a class="noticias-widget-todas" href="<?php echo site_url('noticias') ?>">(ver todas <img src="<?php echo base_url('assets/img/seta_home-ver-todas.png') ?>" alt="<?php echo site_name() . ' Notícias' ?>">)</a></h1>
	<?php foreach ($noticias as $noticia): ?>
		<div class="noticia-link">
			<div class="noticia-data">
				<div class="noticia-dia"><?php echo date('d', $noticia->published) ?></div>
				<div class="noticia-mes"><?php echo month_name($noticia->published, TRUE, TRUE) ?></div>
			</div>
			<div class="noticia-titulo"><?php echo $noticia->titulo ?></div>
			<a href="<?php echo site_url('noticias/?noticia=' . $noticia->id) ?>" class="noticia-mais">saiba mais</a>
			
		</div>
	<?php endforeach ?>
</div>