<?php
/**
* File: admin_paginas.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Calendario
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_paginas extends AuthController
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var [type]
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['module'] = 'paginas';
		$this->load->model('paginas/pagina');
		$exclude_list = array('.', '..');
		$templates_dir =  dirname(__FILE__) . '/../views/templates';
		$this->data['templates'] = array_diff(scandir($templates_dir), $exclude_list);
	}

	/**
	 * Função principal.
	 * 
	 * @return void página principal.
	 */
	public function index()
	{
		$this->lista();
	}

	/**
	 * Lista as páginas cadastradas na base de dados.
	 * 
	 * @return void página de listagem.
	 */
	public function lista()
	{
		if ( ! $this->tank_auth->is_logged_in()) 
		{
			$this->session->set_userdata('bounce_uri',
				$this->uri->uri_string());
			$this->data['main_content'] = 'system/mustLogin';
			$this->data['title'] = site_name() . ' - Painel de Controle';
			$this->load->view('start/templatenonav', $this->data);
		}
		else
		{
			$this->data['paginas'] = $this->pagina->get_all();
			$this->data['conteudo'] = 'paginas/admin_lista';
			$this->load->view('start/template', $this->data);
		}
	}

	/**
	 * Edita a página correspondente ao parâmetro $pagina_id
	 * 
	 * @param int $pagina_id id da página a ser editada.
	 * 
	 * @return void formulário de edição de página.
	 */
	public function editar($pagina_id)
	{
		if ( ! $this->tank_auth->is_logged_in() ) 
		{
			$this->session->set_userdata('bounce_uri',
				$this->uri->uri_string());
			$this->data['main_content'] = 'system/mustLogin';
			$this->data['title'] = site_name() . ' - Painel de Controle';
			$this->load->view('start/templatenonav', $this->data);
		}
		else
		{
			$this->data['pagina'] = $this->pagina->get_conteudo($pagina_id, 'id');
			$this->data['conteudo'] = 'paginas/admin_edita';
			$this->load->view('start/template', $this->data);
		}
	}

	/**
	 * Cadastra uma nova página
	 * 
	 * @return void página com formulário de cadastro
	 */
	public function cadastrar()
	{
		if ( ! $this->tank_auth->is_logged_in() ) 
		{
			$this->session->set_userdata('bounce_uri',
				$this->uri->uri_string());
			$this->data['main_content'] = 'system/mustLogin';
			$this->data['title'] = site_name() . ' - Painel de Controle';
			$this->load->view('start/templatenonav', $this->data);
		}
		else
		{
			$this->data['acao'] = 'cadastrar';
			$this->data['conteudo'] = 'paginas/admin_edita';
			$this->load->view('start/template', $this->data);
		}
	}

	/**
	 * Processa a atualização de uma página
	 * 
	 * @return void redirecionamento
	 */
	public function processa()
	{
		if ( ! $this->tank_auth->is_logged_in() ) 
		{
			$this->session->set_userdata('bounce_uri',
				$this->uri->uri_string());
			$this->data['main_content'] = 'system/mustLogin';
			$this->data['title'] = site_name() . ' - Painel de Controle';
			$this->load->view('start/templatenonav', $this->data);
		}
		else
		{
			$pagina = $this->pagina->get_conteudo($this->input->post( 'id' ), 'id');

			$this->data['acao'] = 'editar';

			if( ! $this->form_validation->run('paginas') )
			{
				$this->data['pagina'] = $pagina;
				$this->data['conteudo'] = 'paginas/admin_edita';
				$this->load->view('start/template', $this->data);
			}
			else
			{
				$post = array();

				foreach($_POST as $chave => $valor)
				{
					$post[$chave] = $valor;
				}
				
				//Checa se houve envio de imagem
				if(strlen($_FILES['imagem']['name'])>0)
				{
					//Tenta persistir a imagem e retorna uma exception caso não persista
					try 
					{
						$upload = $this->_upload_image();
						$post['imagem'] = $upload['file_name'];
					} 
					catch (Exception $e) 
					{
						return $this->_form_error_return($this->data['acao'], $e->getMessage(), $pagina->id, 'id');
					}
				}

				$post['updated'] = time();

				if($this->pagina->change($post))
				{
					$this->session->set_flashdata('success', 'Registro alterado com sucesso');
					redirect('painel/paginas');
				}
				else
				{
					$this->session->set_flashdata('error', 'Não foi possível alterar o registro.
						Tente novamente ou entre em contato com o suporte');
					redirect('painel/paginas/edita/' . $post['id']);
				}
			}
		}
	}

	/**
	 * Retorna o formulário contendo a mensagem de erro
	 * 
	 * @param string $acao  	 [cadastro|edição]
	 * @param string $error 	 mensagem de erro
	 * @param mixed  $value 	 valor a buscar no banco de dados
	 * @param string $identifier identificador do valor passado
	 * 
	 * @return void formulário
	 */
	private function _form_error_return($acao, $error = NULL, $value = NULL, $identifier = NULL)
	{
		$this->data['pagina'] = $this->pagina->get_conteudo($value, $identifier);
		
		if( $error )
			$this->data['error'] = $error;

		$this->data['module'] = 'paginas';
		$this->data['acao'] = $acao;
		$this->data['conteudo'] = 'admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Realiza o upload de uma imagem
	 *
	 * @throws Exception If cant do upload
	 * @return void dados de upload
	 */
	private function _upload_image()
	{
		$config['upload_path'] = './assets/img/paginas/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '8000';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('imagem') )
			throw new Exception($this->upload->display_errors(), 1);

		$this->load->library('image_moo');
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $upload_data = $this->upload->data();
        $file_uploaded = $upload_data['full_path'];
        //Locais dos arquivos alterados
        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

		if ( ! $this->image_moo->load($file_uploaded)
                                ->resize(330,330,FALSE)
                                ->save($new_file,true) )
			throw new Exception("Erro ao redimensionar imagem", 1);
			
		return $this->upload->data();

	}
}
/* End of file admin_paginas.php */
/* Location: ./modules/paginas/controllers/admin_paginas.php */