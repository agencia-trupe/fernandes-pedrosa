<?php
/**
* File: paginas.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Calendario
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Paginas extends MX_Controller
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('paginas/pagina');
	}

	/**
	 * Função principal
	 * 
	 * @return void página principal
	 */
	public function index()
	{
		$this->view();
	}

	/**
	 * Exibe uma página baseando-se no slug passado como parâmetro
	 * 
	 * @param string $slug slug da página
	 * 
	 * @return void página com os conteúdos retornados
	 */
	public function view($slug)
	{
		//busca os dados de uma página cujo slug foi passado como parâmetro
		$dados_pagina = $this->pagina->get_conteudo($slug, 'slug');
		if( ! is_null($dados_pagina) )
		{
			//Variável com os dados da página a ser enviada para a view
			$this->data['dados_pagina'] = $dados_pagina;
			//Carrega a biblioteca de SEO e a inicializa.
			$seo_config = array(
				'title' => $dados_pagina->titulo,
				'description' => $dados_pagina->description,
				);
			$this->load->library('seo', $seo_config);			
			
			$this->data['pagina'] = $slug;
			//Define a view utilizada | Template dinâmica ou fixa.
			if( ! empty($dados_pagina->template) )
			{
				$this->data['conteudo'] = 'paginas/templates/' . $dados_pagina->template;
			}
			else
			{
				$this->data['conteudo'] = 'paginas/templates/index';
			}
			//Carrega a view especificada como parâmetro e exibe a página
			$this->load->view('layout/template', $this->data);
		}
		else
		{
			show_404();
		}
	}
}
/* End of file paginas.php */
/* Location: ./modules/paginas/controllers/paginas.php */