<?php
/**
* File: pagina.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Class_name
 * 
 * @category Sisconeto
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Pagina extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'paginas';

	/**
	 * Retorna todos os registros do banco de dados
	 * 
	 * @return array registros
	 */
	function get_all()
	{
		$pagina = new Pagina();
		$pagina->order_by('titulo', 'ASC')->get();
		$result = array();
		foreach($pagina->all as $pagina)
		{
			$result[] = $pagina;
		}
		if(sizeof($result))
		{
			return $result;
		}
		return FALSE;
	}
	
	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo($valor, $tipo)
	{
		$pagina = new Pagina();
		switch ($tipo) 
		{
			case 'id':
				$pagina->where('id', $valor)->get();
							break;	
			case 'slug':
				$pagina->where('slug', $valor)->get();
							break;
		}
		if($pagina->exists()){
			return $pagina;
		}
		return NULL;
	}

	/**
	 * Insere um novo registro no banco
	 * 
	 * @param array $dados dados do registro.
	 * 
	 * @return boolean status
	 */
	function insert($dados)
	{
		$pagina = new Pagina();
		foreach ($dados as $chave => $valor)
		{
			$pagina->$chave = $valor;
		}
		$pagina->updated = time();
		$insert = $pagina->save();
		if($insert)
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$pagina = new Pagina();
		$pagina->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update_data['updated'] = time();
		$update = $pagina->update($update_data);
		if($update)
		{
			return TRUE;
		}
		return FALSE;
	}
}
/* End of file pagina.php */
/* Location: ./modules/paginas/models/pagina.php */