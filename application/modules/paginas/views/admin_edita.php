<div class="row-fluid">
    <div class="span9">
      <?php if(isset($error)): ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div>
    <?php endif; ?> 
         <legend>Editar página</legend>
    <?=form_open_multipart('paginas/admin_paginas/processa'); ?>
    <?=form_hidden( 'id', $pagina->id ); ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', $pagina->titulo),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
        <br>
    <?=form_label('Texto'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', $pagina->texto),
        'class' => 'tinymce span8'
    )); ?>
    <br>
    <?php if( isset($pagina->imagem) && ! in_array($pagina->slug, array('atuacao', 'perfil', 'home')) ):?>
    <img width="200px" src="<?php echo base_url(); ?>assets/img/paginas/<?php echo $pagina->imagem; ?>" alt="" >
    <?php endif; ?>
    <?php if( ! in_array($pagina->slug, array('atuacao', 'perfil', 'home')) ): ?>
    <div class="control-group">
        <label class="control-label" for="imagem">Alterar Imagem</label>
        <div class="controls">
            <?php echo form_upload('imagem', set_value('imagem')); ?>
            <span class="help-inline"><?php echo form_error('imagem'); ?></span>
        </div>
    </div>
    <?php endif; ?> 
    <br>
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('paginas/admin_paginas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    </div>
</div>