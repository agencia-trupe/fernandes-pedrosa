<div class="conteudo pagina atuacao conteudo-atuacao">
	<h1>
		<div class="interna">
			<?php echo $dados_pagina->titulo ?>
		</div>
	</h1>
	<div class="interna">
		<div class="texto">
			<?php echo $dados_pagina->texto ?>
		</div>
		<?php echo Modules::run('servicos/parcial') ?>
		<div class="clearfix"></div>
		<a href="<?php echo site_url('contato') ?>" class="contato-servicos">
			<span>Entre em contato para maiores esclarecimentos e conheça nosso escritório!</span>
		</a>
		<div class="contato-servicos-shadow"></div>
	</div>
	<div class="clearfix"></div>
</div>