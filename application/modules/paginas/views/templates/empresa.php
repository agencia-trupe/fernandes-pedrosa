<div class="conteudo pagina empresa conteudo-empresa">
	<h1>
		<div class="interna">
			<?php echo $dados_pagina->titulo ?>
		</div>
	</h1>
	<div class="interna">
		<div class="imagem-wrapper">
			<img src="<?php echo base_url('assets/img/paginas/' . $dados_pagina->imagem ) ?>" alt="<?php echo $dados_pagina->titulo ?>">
		</div>
		<div class="texto">
			<?php echo $dados_pagina->texto ?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>