<?php
/**
* File: admin_servicos.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_servicos
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_servicos extends AuthController
{

	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['module'] = 'servicos';
		$this->load->model('servicos/servico');
	}

	/**
	 * Função principal.
	 * 
	 * @return void página principal.
	 */
	public function index()
	{
		$this->lista();
	}

	/**
	 * Lista os registros cadastrados no banco de dados
	 * 
	 * @return void página com lista de registros
	 */
	public function lista()
	{
		$this->data['servicos'] = $this->servico->get_all();
		$this->data['conteudo'] = 'servicos/admin_lista';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Edita o serviço correspondente ao id passado como parâmetro
	 * 
	 * @param int $servico_id id do serviço
	 * 
	 * @return void formulário de ediçã do serviço
	 */
	public function editar($servico_id)
	{

		$this->data['acao'] = 'editar';
		$this->data['servico'] = $this->servico->get_conteudo($servico_id, 'id');
		$this->data['conteudo'] = 'servicos/admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Cadastra um novo serviço
	 * 
	 * @return void página com formulário de cadastro
	 */
	public function cadastrar()
	{
		
		$this->data['acao'] = 'cadastrar';
		$this->data['conteudo'] = 'servicos/admin_edita';
		$this->load->view('start/template', $this->data);
		
	}

	/**
	 * Processa a atualização de um serviço
	 * 
	 * @return void redirecionamento
	 */
	public function processa()
	{
		
		if( ! $this->form_validation->run('servicos'))
		{
			$this->data['acao'] = 'editar';
			$this->data['servico'] = $this->servico->get_conteudo($this->input->post( 'id' ), 'id');
			$this->data['conteudo'] = 'servicos/admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$post = array();
			foreach($this->input->post() as $chave => $valor)
			{
				$post[$chave] = $valor;
			}

			$post['updated'] = time();
			if($this->servico->change($post))
			{
				$this->session->set_flashdata('success', 'Registro alterado com sucesso');
				redirect('painel/atuacao');
			}
			else
			{
				$this->session->set_flashdata('error', 'Não foi possível alterar o registro.
					Tente novamente ou entre em contato com o suporte');
				redirect('painel/atuacao/edita/' . $post['id']);
			}
		}
	}

	/**
	 * Processa o cadastro de um serviço
	 * 
	 * @return void redirecionamento
	 */
	public function processa_cadastro()
	{
		if( ! $this->form_validation->run('servicos') )
		{
			$this->data['acao'] = 'cadastrar';
			$this->data['conteudo'] = 'servicos/admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$this->load->helper('blog');
			$post = array();
			foreach($this->input->post() as $chave => $valor)
			{
				$post[$chave] = $valor;
			}
			
			$post['updated'] = time();

			$post['slug'] = get_slug($post['titulo']);

			if($this->servico->insert($post))
			{
				$this->session->set_flashdata('success', 'Registro incluído com sucesso');
				redirect('painel/atuacao');
			}
			else
			{
				$this->session->set_flashdata('error', 'Não foi possível incluir o registro.
					Tente novamente ou entre em contato com o suporte');
				redirect('painel/atuacao' . $post['id']);
			}
		}
	}

	/**
	 * Deleta um registro do banco de dados
	 * 
	 * @param int $servico_id id do serviço
	 * 
	 * @return void redirecionamento
	 */
	public function deleta_servico($servico_id)
	{
		$apaga = $this->servico->apaga($servico_id);
		if($apaga)
		{
			$this->session->set_flashdata('success', 'Registro removido com sucesso');
			redirect('painel/atuacao');
		}
		else
		{
			$this->session->set_flashdata('error', 'Não foi possível remover o registro.
				Tente novamente ou entre em contato com o suporte');
			redirect('painel/atuacao/');
		}
	}

	/**
	 * Reordena os serviços para exibição
	 * 
	 * @return string status de ordenamento
	 */
	public function sort_servicos()
	{
		$itens = $this->input->post('servico');
		if ($itens)
		{
			$ordenar = $this->servico->ordena($itens);
			if($ordenar)
			{
				echo 'Ordenado';
			}
			else
			{
				echo 'Erro!';
			}
		} 
		else 
		{
			echo 'Erro!';
		}
	}
}
/* End of file admin_servicos.php */
/* Location: ./modules/servicos/controllers/admin_servicos.php */