<?php
/**
* File: servicos.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Serviços
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Servicos extends MX_Controller
{
	
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('servicos/servico');
	}

	/**
	 * Exibe a listagem de serviços
	 * 
	 * @return void listagem de serviços.
	 */
	public function parcial($home = FALSE)
	{
		$this->data['atuacoes'] = $this->servico->get_all();
		if($home)
			$this->data['home'] = TRUE;
		$this->load->view('servicos/parcial', $this->data);
	}
}
/* End of file servicos.php */
/* Location: ./modules/servicos/controllers/servicos.php */