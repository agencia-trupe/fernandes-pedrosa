<?php
/**
* File: servico.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Serviço
 * 
 * @category Sisconeto
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Servico extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'servicos';

	/**
	 * Retorna todos os registros do banco de dados
	 * 
	 * @return array registros
	 */
	function get_all()
	{
		$servico = new Servico();
		$servico->order_by('ordem', 'ASC')->get();
		$result = array();
		foreach($servico->all as $servico)
		{
			$result[] = $servico;
		}
		if(sizeof($result))
		{
			return $result;
		}
		return FALSE;
	}

	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo( $valor, $tipo )
	{
		$servico = new Servico();
		$servico->where( $tipo, $valor )->get();
		if( $servico->exists() ){
			return $servico;
		}
		return NULL;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$servico = new Servico();
		$servico->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update_data['updated'] = time();
		$update = $servico->update($update_data);
		if($update)
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Insere um novo registro no banco
	 * 
	 * @param array $dados dados do registro.
	 * 
	 * @return boolean status
	 */
	function insert($dados)
	{
		$serviços = new Servico();
        $serviços->get();
        $count = $serviços->result_count();

		$servico = new Servico();
		foreach ($dados as $chave => $valor)
		{
			$servico->$chave = $valor;
		}
		$servico->created = time();
		$servico->ordem = $count;
		$insert = $servico->save();
		if($insert)
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Remove um registro do banco de dados
	 * 
	 * @param int $servico_id id do serviço.
	 * 
	 * @return boolean status
	 */
	function apaga($servico_id)
	{
		$servico = new Servico();
		$servico->where('id', $servico_id)->get();
		if($servico->delete())
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Ordena os dados para exibição
	 * 
	 * @param array $dados array com dados para ordenar
	 * 
	 * @return boolean status
	 */
	function ordena($dados)
	{
		$result = array();
		foreach($dados as $chave => $valor)
		{
			$servico = new Servico();
			$servico->where('id', $valor);
			$update_data = array(
				'ordem' => $chave
				);
			if($servico->update($update_data))
			{
				$result[] = $valor;
			}
		}
		if(sizeof($result))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
/* End of file servico.php */
/* Location: ./modules/servicos/models/servico.php */