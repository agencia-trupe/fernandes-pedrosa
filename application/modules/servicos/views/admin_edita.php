<div class="row-fluid">
    <div class="span9">
       <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Serviço</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'servicos/admin_servicos/processa';
                    break;
                
                default:
                    $action = 'servicos/admin_servicos/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?=form_hidden( 'id', ( $acao == 'editar') ? $servico->id : '' ); ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar') ? $servico->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('servicos/admin_servicos/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?> 
    </div>
</div>