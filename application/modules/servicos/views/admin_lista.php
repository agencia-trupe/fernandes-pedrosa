<div class="row-fluid">
    <div class="span9">
        <legend>
            Áreas de atuação <a class="btn btn-info btn-mini" href="<?=site_url('painel/atuacao/cadastrar'); ?>">Novo</a>
            <a href="#" class="ordenar-servicos btn btn-mini btn-info">ordenar áreas</a>
            <a href="#" class="salvar-ordem-servicos hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide servicos-mensagem">
            <span>Para ordenar, clique nos dados da área e arraste até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span7">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($servicos): ?>
                <?php foreach ($servicos as $servico): ?>
                    <tr id="servico_<?php echo $servico->id ?>">
                        <td><?=$servico->titulo; ?></td>
                        <td><?=anchor('painel/atuacao/editar/' . $servico->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                            <?=anchor('painel/atuacao/deleta_servico/' . $servico->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>

        </tbody>
    </table>
    </div>
</div>