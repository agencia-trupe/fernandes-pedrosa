<div class="atuacao-wrapper">
	<ul>
		<?php if ( ! isset($home) ): ?>
		<li class="descricao"><span>Principais Áreas de Atividade</span></li>
		<?php endif ?>
		<?php foreach ($atuacoes as $atuacao): ?>
			<li><?php echo $atuacao->titulo ?></li>
		<?php endforeach ?>
		<?php if (isset($home)): ?>
			<li class="atuacao-contato-home"><a href="<?php echo site_url('contato') ?>">entre em contato</a></li>
		<?php endif ?>
	</ul>
</div>