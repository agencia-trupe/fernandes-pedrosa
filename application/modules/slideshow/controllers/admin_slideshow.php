<?php
/**
* File: admin_slides.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_slideshow
 * 
 * @category Sisconeto
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Admin_slideshow extends AuthController
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('slideshow/slide');
		$this->load->library('form_validation');
		$this->data['module'] = 'slideshow';
	}

	/**
	 * Ação principal
	 * 
	 * @return [type] [description]
	 */
	function index()
	{

		$this->lista();
	}

	/**
	*Lista todos os slides cadastrados atualmente
	*
	* @return [type] [description]
	*/
	function lista()
	{
		$this->load->library('pagination');
		$this->load->library('table');
		$pagination_config = array(
					'base_url'       => site_url() . 'painel/slideshow/lista/',
					'total_rows'     => $this->db->get('slides')->num_rows(),
					'per_page'       => 30,
					'num_links'      => 5,
					'next_link'      => 'próximo',
					'prev_link'      => 'anterior',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['slides'] = $this->slide->
		get_all($pagination_config['per_page'], $this->uri->
			segment(3));

		$this->data['title'] = 'Slides';
		$this->data['conteudo'] = 'slideshow/admin_lista';
		$this->load->view('start/template', $this->data);      
	}
	
	/**
	 * Adiciona uma nova notícia
	 *
	 * @return void formulário de cadastro
	 */
	function add()
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor) 
		{
			$post[$chave] = $valor;
		}
		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'cadastra';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! count( $post ) )
		{
			$this->data['module'] = 'slides';
			$this->data['conteudo'] = 'admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$published = explode('/', $post['published']);
			$post['published'] = strtotime($published[1] . '/' . $published[0] . '/' . $published[2]);
			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('slides') )
				return $this->_form_error_return($this->data['acao']);

			//Checa se houve envio de imagem
			if(strlen($_FILES['imagem']['name'])>0)
			{
				//Tenta persistir a imagem e retorna uma exception caso não persista
				try 
				{
					$upload = $this->_upload_image();
					$post['imagem'] = $upload['file_name'];
				} 
				catch (Exception $e) 
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage());
				}
			}

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try 
			{
				$post['created'] = $_SERVER['REQUEST_TIME'];
				$post['user_id'] = $this->tank_auth->get_user_id();
				$this->slide->insert($post);
				redirect('painel/slideshow');
			} 
			catch (Exception $e) 
			{
				if($upload)
					unlink($upload['full_path']);
				return $this->_form_error_return($this->data['acao'], $e->getMessage());
			}
		}
	}

	/**
	 * Edita uma notícia.
	 *
	 * @param [int] $slide_id [description]
	 * 
	 * @return [mixed]     [description]
	 */
	function edit($slide_id = NULL)
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor) 
		{
			$post[$chave] = $valor;
		}

		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'editar';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! $post )
		{
			try 
			{
				$this->data['slide'] = $this->slide->get_conteudo($slide_id, 'id');
			} 
			catch (Exception $e) 
			{
				//Retorna a página de listagem caso nenhuma página corresponda ao id fornecido
				$this->session->set_flashdata('error', $e->getMessage());
				redirect('painel/slideshow');
			}
			$this->data['acao'] = 'editar';
			$this->data['conteudo'] = 'admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{

			$slide = $this->slide->get_conteudo($post['id'], 'id');

			//Define a imagem atual, que será apagada em caso de novo upload de imagem
			$old_image = $slide->imagem;

			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('slides') )
				return $this->_form_error_return($this->data['acao'], NULL, $post['id'], 'id');

			//Checa se houve envio de imagem
			if(strlen($_FILES['imagem']['name'])>0)
			{
				//Tenta persistir a imagem e retorna uma exception caso não persista
				try 
				{
					$upload = $this->_upload_image();
					$post['imagem'] = $upload['file_name'];
				} 
				catch (Exception $e) 
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage(), $slide->id, 'id');
				}
			}

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try 
			{
				$post['updated'] = $_SERVER['REQUEST_TIME'];
				$post['user_id'] = $this->tank_auth->get_user_id();
				$this->slide->change($post);
				if($upload)
					unlink($upload['file_path'] . $old_image);
				redirect('painel/slideshow');
			} 
			catch (Exception $e) 
			{
				if($upload)
					unlink($upload['full_path']);
				return $this->_form_error_return($this->data['acao'], $e->getMessage(), $post['id'], 'id');
			}
		}
	}

	/**
	 * Deleta uma notícia
	 *  
	 * @return void redirect
	 */
	function delete()
	{
		$post = $this->input->post();
		try 
		{
			$imagem = $this->slide->apaga($post['id']);
		} 
		catch (Exception $e) 
		{
			$this->session->set_flashdata( 'error', $e->getMessage() );
			redirect('painel/slideshow');
		}
		unlink('./assets/img/slides/' . $imagem);
		$this->session->set_flashdata( 'success', 'Registro removido' );
		redirect('painel/slideshow');

	}

	/**
	 * Retorna o formulário contendo a mensagem de erro
	 * 
	 * @param string $acao  	 [cadastro|edição]
	 * @param string $error 	 mensagem de erro
	 * @param mixed  $value 	 valor a buscar no banco de dados
	 * @param string $identifier identificador do valor passado
	 * 
	 * @return void formulário
	 */
	private function _form_error_return($acao, $error = NULL, $value = NULL, $identifier = NULL)
	{
		if( $acao === 'editar' )
			$this->data['slide'] = $this->slide->get_conteudo($value, $identifier);
		
		if( $error )
			$this->data['error'] = $error;

		$this->data['module'] = 'slides';
		$this->data['acao'] = $acao;
		$this->data['conteudo'] = 'admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Realiza o upload de uma imagem
	 *
	 * @throws Exception If cant do upload
	 * @return void dados de upload
	 */
	private function _upload_image()
	{
		$config['upload_path'] = './assets/img/slides/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '8000';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('imagem') )
			throw new Exception($this->upload->display_errors(), 1);

		$this->load->library('image_moo');
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $upload_data = $this->upload->data();
        $file_uploaded = $upload_data['full_path'];
        //Locais dos arquivos alterados
        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

		if ( ! $this->image_moo->load($file_uploaded)
                                ->resize_crop(650,358,FALSE)
                                ->save($new_file,true) )
			throw new Exception("Erro ao redimensionar imagem", 1);
			
		return $this->upload->data();

	}
}

/* End of file admin_slides.php */
/* Location: ./modules/slides/controllers/admin_slides.php */