<?php
/**
* File: slide.php
* 
* PHP version 5.3
*
* @category Sisconeto
* @package  Sisconeto
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Slide
 * 
 * @category Sisconeto
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Slide extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'slides';

	/**
	 * Retorna todos os registros do banco de dados
	 * 
	 * @return array registros
	 */
	function get_all($limit = NULL, $offset = NULL)
	{
		$slide = new slide();
		$slide->order_by('ordem', 'ASC');
		if($limit || $offset)
		{
			$slide->get($limit, $offset);
		} 
		else
		{
			$slide->get();
		}
		
		$result = array();
		foreach($slide->all as $slide)
		{
			$result[] = $slide;
		}

		return (sizeof($result)) ? $result : NULL;
		
	}

	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo( $valor, $tipo )
	{
		$slide = new slide();
		$slide->where( $tipo, $valor )->get();
		if( ! $slide->exists() )
			throw new Exception("Nenhum registro corresponde à esse id", 1);

		return $slide;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$slide = new slide();
		$slide->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update = $slide->update($update_data);
		if( ! $update )
			throw new Exception("Erro ao atualizar registro", 1);
			
		return TRUE;
	}

	/**
	 * Insere um novo registro no banco
	 * 
	 * @param array $dados dados do registro.
	 * 
	 * @return boolean status
	 */
	function insert($dados)
	{
		$slide = new slide();
		foreach ($dados as $chave => $valor)
		{
			$slide->$chave = $valor;
		}
		$insert = $slide->save();

		if( ! $insert )
			throw new Exception("Erro ao inserir notícia.", 1);
			
		return true;
	}

	/**
	 * Remove um registro do banco de dados
	 * 
	 * @param int $slide_id id do serviço.
	 * 
	 * @return boolean status
	 */
	function apaga($slide_id)
	{
		$slide = new slide();
		$slide->where('id', $slide_id)->get();
		$imagem = $slide->imagem;
		if( ! $slide->delete() )
			throw new Exception("Erro ao apagar registro", 1);
			

		return $imagem;
	}

	function ordena($dados)
	{
		$result = array();
		foreach($dados as $chave => $valor)
		{
			$slide = new Slide();
			$slide->where('id', $valor);
			$update_data = array(
				'ordem' => $chave
				);
			if($slide->update($update_data))
			{
				$result[] = $valor;
			}
		}
		if(sizeof($result))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
/* End of file slide.php */
/* Location: ./modules/slideshow/models/slide.php */