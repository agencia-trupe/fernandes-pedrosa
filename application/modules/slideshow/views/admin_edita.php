<div class="span9">
<?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
<legend><?php echo $acao == 'editar' ? 'Editar slideshow' : 'Novo Slide' ;?></legend>
    <?php echo isset($error) ? $error : ''; ?>
    <?php
    echo form_open_multipart(($acao == 'editar') ? 'painel/slideshow/edit' : 'painel/slideshow/add', 'class="well"'); 
    ?>

    
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $slide->id));
    ?>
  <?php endif; ?>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="titulo">Título</label>
            <div class="controls">
              <?php echo form_input(array('name'=>'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $slide->titulo : ''), 'class'=>'span9', 'maxlength'=>45)); ?>
              <span class="help-inline"><?php echo form_error('titulo'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="link">Link</label>
            <div class="controls">
              <?php echo form_input('link', set_value('link', $acao == 'editar' ? $slide->link : ''), 'class="span9"'); ?>
              <span class="help-inline"><?php echo form_error('link'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="texto">Texto</label>
            <div class="controls">
              <?php echo form_textarea('texto', set_value('texto', $acao == 'editar' ? $slide->texto : ''), 'class="span9 tinymce"'); ?>
              <span class="help-inline"><?php echo form_error('texto'); ?></span>
            </div>
            <span>Limite de caracteres: </span><div id="characterCounter"></div>
      </div>
      <?php if(isset($slide->imagem)):?>
    <img width="200px" src="<?php echo base_url(); ?>assets/img/slides/<?php echo $slide->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="imagem">Alterar Imagem</label>
        <div class="controls">
            <?php echo form_upload('imagem', set_value('imagem')); ?>
            <span class="help-inline"><?php echo form_error('imagem'); ?></span>
        </div>
    </div>
  </div>
  <?php echo form_submit('', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>  