<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="span11">
        <legend>
            Slides 
            <a href="<?=site_url('painel/slideshow/add/'); ?>" class="btn btn-action btn-mini">Novo Slide</a>
            <a href="#" class="ordenar-slides btn btn-mini btn-info">ordenar slides</a>
            <a href="#" class="salvar-ordem-slides hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide slides-mensagem">
            <span>Para ordenar, clique nos dados do slide e arraste até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Título</th><th>texto</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
            <tbody>
                <?php foreach ($slides as $slide): ?>
                <tr id="slide_<?php echo $slide->id ?>">
                    <td><?=$slide->titulo; ?></td>
                    <td><?=$slide->texto; ?></td>
                    <td><a href="<?=site_url('painel/slideshow/edit/' . $slide->id); ?>" class="btn btn-info btn-mini">Editar</a>
                        <?php echo form_open('painel/slideshow/delete', 'style="display:inline"') ?>
                            <?php echo form_hidden('id', $slide->id) ?>
                            <?php echo form_submit('', 'apagar', 'class="btn btn-danger btn-mini"') ?>
                        <?php echo form_close() ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </thead>
    </table>
</div>