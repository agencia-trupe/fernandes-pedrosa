<div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <?php if($this->tank_auth->is_role('admin')) :?>
              <li class="nav-header">Tipos de registro</li>
              <li><a href="<?php echo base_url(); ?>sys/logs/lista/acesso"><i class="icon-list-alt"></i>Registros de Acesso</a></li>
              <li><a href="<?php echo base_url(); ?>sys/logs/lista/cadastro"><i class="icon-list-alt"></i>Registros de Cadastro</a></li>
              <li><a href="<?php echo base_url(); ?>sys/logs/lista/erro"><i class="icon-list-alt"></i>Registros de Erro</a></li>
              <li><a href="<?php echo base_url(); ?>sys/logs/lista/retorno"><i class="icon-list-alt"></i>Registros de Retorno</a></li>
             
              <?php endif; ?>
              
            </ul>
          </div><!--/.well -->
        </div><!--/span-->