            </div>
            <div class="clearfix"></div>
        <footer>
            <?php echo Modules::run('contato/footer') ?>
            <div class="copy">
                <div class="interna">
                    <div class="copy-content left">
                        &copy;2013 - Fernandes & Pedrosa - Todos os direitos reservados.
                    </div>
                    <div class="author right">
                        <a href="http://trupe.net" target="_blank" class="trupe">
                            Criação de Sites: Trupe Agência Criativa <img src="<?php echo base_url('assets/img/trupe.png') ?>" alt="Criação de Sites: Trupe Agência Criativa">
                        </a>
                        </div>
                </div>

            </div>
        </footer>
    </div>
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url('assets/js/vendor/jquery-1.8.2.min.js') ?>"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/main.js') ?>"></script>
        <?php if($pagina === 'novidades'): ?>
        <script id="noticia_template" type="text/html">
            <div class="conteudo-inner">
                <header>
                <div class="data">{{data}}</div>
                <div class="separador-data"></div>
                <h1>{{titulo}}</h1>
                <div class="resumo">{{{resumo}}}</div>
                <div class="separador-resumo"></div>
                </header>
                {{#imagem}}
                    <div class="noticia-image-wrapper">
                        <img src="/assets/img/noticias/{{imagem}}">
                    </div>
                {{/imagem}}
                {{{texto}}}
            </div>
        </script>
        <?php endif; ?>
        <script>
            var _gaq=[['_setAccount','  '],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
