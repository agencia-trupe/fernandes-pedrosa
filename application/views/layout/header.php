<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">
        <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js') ?>"></script>
        <!--[if IE]>
            <style type="text/css">
               .projeto-thumb-hover, .outro-projeto .hover { 
                   background:transparent;
                   filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#CC333333,endColorstr=#CC333333); 
                   zoom: 1;
                } 
            </style>
        <![endif]-->
        <title><?=$this->seo->get_title(); ?></title>
    </head>
    <body class="<?php echo $pagina ?>">
        <div class="clearfix"></div>
        <div class="wrapper">
            
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <header>
            <div class="interna">
                <div class="clearfix"></div>
                <div class="marca">
                    <img src="<?php echo base_url('assets/img/fernandes-pedrosa.png') ?>" alt="Sisconeto Advogados Associados">
                </div>
                <nav>
                    <ul>
                        <li><a class="<?php echo ($pagina === 'home') ? 'active' : '' ?>" href="<?php echo site_url() ?>">Home</a></li>
                        <li><a class="<?php echo ($pagina === 'empresa') ? 'active' : '' ?>" href="<?php echo site_url('empresa') ?>">Empresa</a></li>
                        <li><a class="<?php echo ($pagina === 'perfil') ? 'active' : '' ?>" href="<?php echo site_url('perfil') ?>">Perfil</a></li>
                        <li><a class="<?php echo ($pagina === 'atuacao') ? 'active' : '' ?>" href="<?php echo site_url('atuacao') ?>">Áreas de Atuação</a></li>
                        <li><a class="<?php echo ($pagina === 'noticias') ? 'active' : '' ?>" href="<?php echo site_url('noticias') ?>">Notícias</a></li>
                        <li><a class="<?php echo ($pagina === 'contato') ? 'active' : '' ?>" href="<?php echo site_url('contato') ?>">Contato</a></li>
                    </ul>
                </nav>
                <div class="detalhe-header"></div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </header>