$(window).load(function() {
    //Margem do quem-somos
    instTexto = $('.conteudo-institucional .texto').outerHeight();
    $('.quem-somos').css('margin-top', - instTexto - 70 + 'px');
    tabsNav = $('.ui-tabs-nav').outerHeight();
    $('.atuacao-tab').css('margin-top', - tabsNav - 75 + 'px');
    $('.home-noticia').first().css('margin-left', 0);
});


function getNoticia(noticiaId, inner) {
    $.getJSON('noticias/detalhe/' + noticiaId, function(result){
        var noticia_template = ich.noticia_template(result);
        if(inner)
        {
            $('.conteudo-wrapper').fadeOut('slow', function(){
                $(this).empty();
                $(this).html(noticia_template);
                $(this).fadeIn('slow');
            })
        } else {
            $('.conteudo-wrapper').html(noticia_template);
        } 
    });
}

if($('body').hasClass('home')) { 
    $.getJSON('/sisconeto/index.php/home/template', function (template) {
        ich.addTemplate(template.name, template.template);
    });
}


function homeNewsSlider() {
    var resultDiv = $('.home-noticias-content');
    var newsList = [];
    var randomEntry = [];

    //Faz a chamada para o array JSON com os dados das notícias
    $.getJSON(location.protocol + '//' + location.host + '/sisconeto/index.php/noticias/json', function(result) {
      newsList = result;
      getRandomNewsCouple(newsList);
      setInterval(function(){
          resultDiv.fadeOut('slow', function(){
            resultDiv.empty();
            getRandomNewsCouple(newsList, function(){
              resultDiv.fadeIn(2000);
            });
          });
          }, 8000);
    });
}

//Obtem um par de notícias aleatórias de uma fonte JSON
function getRandomNewsCouple(source, callback) {
    
    randomEntry = source[Math.floor(Math.random() * source.length)];
    $.each(randomEntry, function(i, item) {
        var singleNews = ich.newsWidget(randomEntry[i]);
        $('.home-noticias-content').append(singleNews);
    });
    if(callback)
    {
        callback();
    }
    $('.home-noticia').first().css('margin-left', 0);
}


$(function() {
    //primeiro slider de novidades ativo
    $('.novidades-nav-slider > .novidades-nav-slide a').first().addClass('active');

     $('.perfil-imagem img').corner('110px');
    //Notícias Home
    if($('body').hasClass('home')) {
        //homeNewsSlider();
    }
    //slider novidades
    var homeSlider = $('.home-slider').bxSlider({
        controls:false,
        auto: false,
    });

    if($('body').hasClass('novidades')) {
        var uri = new URI;
        var search = uri.search(true);
        if(search.page){
            var page = search.page;
        } else {
            var page = 0;
        }
        if(search.noticia){
            var noticia = search.noticia;
            getNoticia(noticia);
            $('.nav-wrapper a').removeClass('active');
            $('.nav-wrapper a[data-id=' + noticia + ']').addClass('active');
        } else {
            var noticia = $('.novidades-nav-slide a:first-child').data('id');
        }


        var novidadesSlider = $('.novidades-nav-slider').bxSlider( {
            mode:'vertical',
            infiniteLoop: false,
            pager: false,
            prevText: 'recentes',
            nextText: 'anteriores',
            startSlide: page,
        });

        if (novidadesSlider.getSlideCount() < 2) {
            $('.bx-controls').hide();
        }

        History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
            // Log the State
            var State = History.getState(); // Note: We are using History.getState() instead of event.state
            uri = new URI(State.url);
            stateSearch = uri.search(true);

            if(stateSearch.page){
                var page = stateSearch.page;
            } else {
                var page = 0;
            }

            if(stateSearch.noticia){
                var stateNoticia = stateSearch.noticia;
            } else {
                var stateNoticia = $('.novidades-nav-slide a:first-child').data('id');
            }
            getNoticia(stateNoticia, true);
            novidadesSlider.goToSlide(stateSearch.page);
        });

        navLink = $('.nav-link');
        navLink.click(function(){
            var navId = $(this).data('id');
            var pageId = $(this).data('page');
            History.pushState(null, null, "?page=" + pageId + '&noticia=' + navId);
            $('.nav-wrapper a').removeClass('active');
            $(this).addClass('active');
            return false;
        });
    }

    //Adiciona a barra separadora
    $('.conteudo-institucional h2').append('<div class="separador-titulo"></div>');

    $(".telefone_form").focus(function () {
        $(this).mask("(99) 9999-9999?9");
    });

    $(".telefone_form").focusout(function () {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    });

    //Tabs atuação
    $('.atuacao-tabs').tabs();

    var tabsUi = $('.atuacao-tabs');
    tabsUi.tabs();

    // Add hash to URL when tabs are clicked
    tabsUi.find('> ul a').click(function() {
        history.pushState(null, null, $(this).attr('href'));
    });

    // Switch to correct tab when URL changes (back/forward browser buttons)
    $(window).bind('hashchange', function() {
        if (location.hash !== '') {
            var tabNum = $('a[href=' + location.hash + ']').parent().index();
            tabsUi.tabs('option', 'active', tabNum);
        } else {
            tabsUi.tabs('option', 'active', 0);
        }
    });

    //Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
    //contato - contato
    $('#contato-form').submit(function() {
        $('#message').html('Enviando...'); 
        var form_data = {
            nome : $('.nome_form').val(),
            email : $('.email_form').val(),
            telefone : $('.telefone_form').val(),
            mensagem : $('.mensagem_form').val(),
            ajax : '1'
        };
        $.ajax({
            url: location.protocol + "//" 
                    + location.host + "/contato/ajax_check",
            type: 'POST',
            async : false,
            data: form_data,
            success: function(msg) {
                alert(msg);
                if(msg == 'Mensagem enviada com sucesso!'){
                    $('.nome_form, .email_form, .telefone_form, .mensagem_form')
                        .val('');
                }
            }
        });
        return false;
    });
});